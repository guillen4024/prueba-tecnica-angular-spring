CREATE TABLE public.pais (
	pais_id varchar(2) PRIMARY KEY,
	pais_nombre varchar(100) not null,
	pais_estado bool not null default true
);
insert into public.pais (pais_id, pais_nombre) values ('CO', 'Colombia');
insert into public.pais (pais_id, pais_nombre) values ('US', 'Estados Unidos');

CREATE TABLE public.tipo_documento (
	tdc_id varchar(2) PRIMARY KEY,
	tdc_nombre varchar(100) not null,
	tdc_estado bool not null default true
);
insert into public.tipo_documento (tdc_id, tdc_nombre) values ('CC', 'C�dula de Ciudadan�a');
insert into public.tipo_documento (tdc_id, tdc_nombre) values ('CE', 'C�dula de Extranjer�a');
insert into public.tipo_documento (tdc_id, tdc_nombre) values ('PP', 'Pasaporte');
insert into public.tipo_documento (tdc_id, tdc_nombre) values ('PE', 'Permiso Especial');

CREATE TABLE public.area (
	area_id serial PRIMARY KEY,
	area_nombre varchar(100) not null,
	area_estado bool not null default true
);
insert into public.area (area_nombre) values ('Administraci�n');
insert into public.area (area_nombre) values ('Financiera');
insert into public.area (area_nombre) values ('Compras');
insert into public.area (area_nombre) values ('Infraestructura');
insert into public.area (area_nombre) values ('Operaci�n');
insert into public.area (area_nombre) values ('Talento Humano');
insert into public.area (area_nombre) values ('Servicios Varios');
insert into public.area (area_nombre) values ('Otras');

CREATE TABLE public.usuario (
	usu_id serial PRIMARY KEY,
	usu_apellido1 varchar(20) NOT NULL,
	usu_apellido2 varchar(20) NULL,
	usu_nombre1 varchar(20) NOT NULL,
	usu_nombre2 varchar(50) NULL,
	pais_id varchar(2) NULL,
	tdc_id varchar(2) NULL,
	usu_identificacion varchar(20) UNIQUE NOT NULL,
	usu_correo varchar(300) UNIQUE NOT NULL,
	usu_fechaingreso date NOT NULL,
	area_id int2 NULL,
	usu_estado bool NOT NULL DEFAULT true,
	usu_fechahoraregistro timestamp(0) NOT NULL DEFAULT now(),
	usu_fechahoraedicion timestamp(0) NOT NULL DEFAULT now(),
	FOREIGN KEY (pais_id) REFERENCES pais (pais_id),
	FOREIGN KEY (tdc_id) REFERENCES tipo_documento (tdc_id),
	FOREIGN KEY (area_id) REFERENCES area (area_id)
);

create index usu_idex1 on public.usuario(usu_identificacion);

INSERT INTO public.usuario
(usu_apellido1, usu_apellido2, usu_nombre1, usu_nombre2, pais_id, tdc_id, usu_identificacion, usu_correo, usu_fechaingreso, area_id, usu_estado, usu_fechahoraregistro)
VALUES('DE LA CALLE', 'MORA', 'JUAN', 'GUILLERMO', 'CO', 'CC', '1234567890', 'juan.delacalle@cidenet.com.co ', '06/03/2021', 1, true, now());



SELECT usu_id, usu_apellido1, usu_apellido2, usu_nombre1, usu_nombre2, pais_id, tdc_id, usu_identificacion, usu_correo, usu_fechaingreso, area_id, usu_estado, usu_fechahoraregistro
FROM public.usuario;
