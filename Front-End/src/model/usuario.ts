export interface usuario {
    "id": number,
    "usu_apellido1": String,
    "usu_apellido2": String,
    "usu_nombre1": String,
    "usu_nombre2": String,
    "pais_id": String,
    "pais_nombre"?: String, 
    "tdc_id": String,
    "tdc_nombre"?: String,
    "usu_identificacion": String,
    "usu_correo": String,
    "usu_fechaingreso": Date,
    "area_id": number,
    "area_nombre"?: String,
    "usu_estado": boolean,
    "usu_fechahoraregistro": Date,
    "usu_fechahoraedicion": Date
}