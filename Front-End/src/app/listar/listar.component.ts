import { Component, OnInit, ViewChild } from '@angular/core';

import { Servicios } from '../app.servicios.service';
import { usuario } from '../../model/usuario';
import Swal from 'sweetalert2';

import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.scss']
})
export class ListarComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private servicios: Servicios) { }

  /*Declaración e inicialización de variables globales*/
  tipodoc: any; area: any; pais: any;
  control: boolean = false; usuario: any;

  displayedColumns: string[] = ['usu_apellido1', 'usu_apellido2', 'usu_nombre1', 'usu_nombre2', 'tdc_nombre', 'usu_identificacion',
    'usu_correo', 'pais_nombre', 'area_nombre', 'usu_fechaingreso', 'usu_estado', 'usu_fechahoraregistro', 'usu_fechahoraedicion', 'editar', 'eliminar'];
  dataSource!: MatTableDataSource<usuario>;

  /*----------------------------------------------------------------------------------*/

  ngOnInit(): void {
    /*Consumo de los servicios necesarios para la visualización de la información. 
    Se llaman en cascada o callback para evitar problemas con la asincronia */
    this.servicios.Get('area').subscribe(res => {
      this.area = res;
      this.servicios.Get('pais').subscribe(res => {
        this.pais = res;
        this.servicios.Get('tipodocumento').subscribe(res => {
          this.tipodoc = res;
          this.servicios.Get('usuario').subscribe((res: usuario[]) => {
            res.forEach(e => {
              e.area_nombre = this.area.find((a: any) => a.area_id === e.area_id).area_nombre;
              e.pais_nombre = this.pais.find((p: any) => p.pais_id === e.pais_id).pais_nombre;
              e.tdc_nombre = this.tipodoc.find((td: any) => td.tdc_id === e.tdc_id).tdc_nombre;
            });
            this.dataSource = new MatTableDataSource<usuario>(res);
            this.dataSource.paginator = this.paginator;
          });
        });
      });
    });
  }
  /*Función para filtrar la información por cualquier parametro*/
  aplicarFiltro(Value: any) {
    let filterValue = Value.target.value.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  /*Función que envia el usuario que recibe al componente "editar" y habilita la vista de dicho componente*/
  editar(usu: usuario) {
    this.usuario = usu;
    this.control = !this.control;
  }

  /*Función que se ejecuta al finalizar la edición de un registro y es ewncargada de cerrar la vista del compònente "editar"*/
  editado(e: any) {
    this.control = !this.control;
    this.ngOnInit();
  }

  /*Función que elimina al usuario que recibe*/
  eliminar(usu: usuario) {
    Swal.fire({
      title: 'Eliminar?',
      text: "Está seguro de que desea eliminar el empleado? ",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si',
      cancelButtonText: 'No'
    }).then((result) => {
      if (result.isConfirmed) {
        this.servicios.Delete('usuario', usu).subscribe(res => {
          Swal.fire(
            'Eliminado!',
            'El empleado ha sido eliminado.',
            'success'
          ).then(() => {
            this.ngOnInit();
          });
        });
      }
    })
  }

}
