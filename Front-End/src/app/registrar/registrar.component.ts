import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Servicios } from '../app.servicios.service';

import Swal from 'sweetalert2';
import * as moment from 'moment';

@Component({
  selector: 'app-registrar',
  templateUrl: './registrar.component.html',
  styleUrls: ['./registrar.component.scss']
})
export class RegistrarComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private servicios: Servicios) {
    /*Variables para controlar la fecha de ingreso mayor y menor (maxDate=fecha actual, minDate=un mes atras a la fecha actual)*/
    this.minDate = new Date(moment().add(-1, 'month').toDate());
    this.maxDate = new Date(moment().toDate());
  }

  form: any;
  minDate!: Date; maxDate!: Date;
  tipodoc: any; area: any; pais: any;
  regex = /^([^ÁÉÍÓÚÑ]+)(\D+)([A-Z ]*)$/;
  controlFecha: Boolean = false;

  ngOnInit(): void {
    /*Llamado a los servicios que llenan las listas desplegables*/
    this.servicios.Get('area').subscribe(res => { this.area = res });
    this.servicios.Get('pais').subscribe(res => { this.pais = res });
    this.servicios.Get('tipodocumento').subscribe(res => { this.tipodoc = res });

    /*Definicion de los compos del formulario y sus validaciones*/
    this.form = this.formBuilder.group({
      usu_apellido1: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_apellido2: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_nombre1: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_nombre2: [null, [Validators.pattern(this.regex)]],
      pais_id: [null, [Validators.required]],
      tdc_id: [null, [Validators.required]],
      usu_identificacion: [null, [Validators.required, Validators.pattern(/[A-Za-z0-9]+$/)]],
      usu_correo: [null],
      usu_fechaingreso: [null, [Validators.required]],
      area_id: [null, [Validators.required]],
      usu_estado: [true],
      usu_fechahoraregistro: [null],
      usu_fechahoraedicion: [null]
    });
  }

  submit() {
    console.log(this.form.get('usu_nombre2').value);
    if (this.form.valid) {
      this.form.controls['usu_fechaingreso'].setValue(moment(this.form.get('usu_fechaingreso').value).format('YYYY-MM-DD'));
      this.form.controls['usu_fechahoraregistro'].setValue(moment().format('YYYY-MM-DDThh:mm:ssZ'));
      this.form.controls['usu_apellido1'].setValue(this.form.get('usu_apellido1').value.toUpperCase());
      this.form.controls['usu_apellido2'].setValue(this.form.get('usu_apellido2').value.toUpperCase());
      this.form.controls['usu_nombre1'].setValue(this.form.get('usu_nombre1').value.toUpperCase());
      
      if (this.form.get('usu_nombre2').value !== null) {
        this.form.controls['usu_nombre2'].setValue(this.form.get('usu_nombre2').value.toUpperCase());
      }
      this.servicios.Get(`usuario/${this.form.get('usu_identificacion').value}`).subscribe(resp => {
        if (resp.usu_identificacion === null) {
          this.servicios.Post('usuario', this.form.value).subscribe(res => {
            if (res.id) {
              Swal.fire({
                icon: 'success',
                title: 'Empleado registrado!',
                text: `Correo: ${res.usu_correo}, fecha de registro: ${moment(res.usu_fechahoraregistro).format('DD/MM/YYYY hh:mm:ss')}`
              }).then(() => {
                this.form.reset();
              });
            }
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: `Ya existe un empleado registrado con el número de identificación: ${resp.usu_identificacion}`
          })
        }
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Falta ingresar datos obligatorios'
      })
    }
  }

}
