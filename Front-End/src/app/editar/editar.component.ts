import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { Servicios } from '../app.servicios.service';
import Swal from 'sweetalert2';
import * as moment from 'moment';
import { usuario } from '../../model/usuario';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.scss']
})
export class EditarComponent implements OnInit {

  @Input() usuario!: usuario;
  @Output() actualizado = new EventEmitter<boolean>();

  constructor(private servicios: Servicios, private formBuilder: FormBuilder) { }

  tipodoc: any; area: any; pais: any;
  regex = /^([^ÁÉÍÓÚÑ]+)(\D+)([A-Z ]*)$/;
  form: any;
  minDate!: Date; maxDate!: Date;

  ngOnInit(): void {
    /*Llamado a los servicios que llenan las listas desplegables*/
    this.servicios.Get('area').subscribe(res => { this.area = res });
    this.servicios.Get('pais').subscribe(res => { this.pais = res });
    this.servicios.Get('tipodocumento').subscribe(res => { this.tipodoc = res });

    /*Definicion de los compos del formulario y sus validaciones*/
    this.form = this.formBuilder.group({
      usu_apellido1: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_apellido2: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_nombre1: [null, [Validators.required, Validators.pattern(this.regex)]],
      usu_nombre2: [null, [Validators.pattern(this.regex)]],
      pais_id: [null, [Validators.required]],
      tdc_id: [null, [Validators.required]],
      usu_identificacion: [null, [Validators.required, Validators.pattern(/[A-Za-z0-9]+$/)]],
      usu_correo: [null],
      usu_fechaingreso: [null, [Validators.required]],
      area_id: [null, [Validators.required]],
      usu_estado: [true],
      usu_fechahoraregistro: [null],
      usu_fechahoraedicion: [null]
    });

    this.form.patchValue({
      usu_apellido1: this.usuario.usu_apellido1,
      usu_apellido2: this.usuario.usu_apellido2,
      usu_nombre1: this.usuario.usu_nombre1,
      usu_nombre2: this.usuario.usu_nombre2,
      pais_id: this.usuario.pais_id,
      tdc_id: this.usuario.tdc_id,
      usu_identificacion: this.usuario.usu_identificacion,
      usu_correo: this.usuario.usu_correo,
      usu_fechaingreso: this.usuario.usu_fechaingreso,
      area_id: this.usuario.area_id,
      usu_estado: this.usuario.usu_estado,
      usu_fechahoraregistro: this.usuario.usu_fechahoraregistro,
      usu_fechahoraedicion: null
    });
  }

  retornar(msg: boolean) {
    this.actualizado.emit(msg)
  }

  submit() {
    if (this.form.valid) {
      if (this.form.get('usu_nombre1').value.toUpperCase().trim() !== this.usuario.usu_nombre1.toUpperCase().trim() ||
        this.form.get('usu_apellido1').value.toUpperCase().trim() !== this.usuario.usu_apellido1.toUpperCase().trim()
      ) {
        this.form.controls['usu_correo'].setValue(null);
      }
      this.form.controls['usu_fechaingreso'].setValue(moment(this.form.get('usu_fechaingreso').value).format('YYYY-MM-DD'));
      this.form.controls['usu_fechahoraedicion'].setValue(moment().format('YYYY-MM-DDThh:mm:ssZ'));
      this.form.controls['usu_apellido1'].setValue(this.form.get('usu_apellido1').value.toUpperCase());
      this.form.controls['usu_apellido2'].setValue(this.form.get('usu_apellido2').value.toUpperCase());
      this.form.controls['usu_nombre1'].setValue(this.form.get('usu_nombre1').value.toUpperCase());
      this.form.controls['usu_nombre2'].setValue(this.form.get('usu_nombre2').value.toUpperCase());
      this.servicios.Put(`usuario/${this.usuario.id}`, this.form.value).subscribe(res => {
        if (res.id) {
          Swal.fire({
            icon: 'success',
            title: 'Empleado actualizado!',
            text: `Correo: ${res.usu_correo}, fecha de actualización: ${moment(res.usu_fechahoraedicion).format('DD/MM/YYYY hh:mm:ss')}`
          }).then(() => {
            this.retornar(true);
          });
        }
      });
    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Falta ingresar datos obligatorios'
      })
    }
  }

}
