import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

const url = 'http://localhost:8080/api/';

@Injectable({
  providedIn: 'root'
})
export class Servicios {

  constructor(private http: HttpClient) { }

  Get(rest: string): Observable<any> {
    return this.http.get(`${url}${rest}`);
  }

  Post(rest: string, data: any): Observable<any> {
    return this.http.post(`${url}${rest}`, data);
  }

  Delete(rest: string, data: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' }), body: data
    };
    return this.http.delete(`${url}${rest}`, httpOptions);
  }

  Put(rest: string, data: any): Observable<any> {
    return this.http.put(`${url}${rest}`, data);
  }

}
