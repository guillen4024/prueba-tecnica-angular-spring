package com.empleados.cidenet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpleadosApplication {
	
	private static final Logger logger = LoggerFactory.getLogger(EmpleadosApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(EmpleadosApplication.class, args);
        logger.error("ERROR - Level Log Message");//Print a ERROR Logger Msg
		logger.info("INFO - Level Log Message"); //Print a Info Logger Msg
        logger.warn("WARN - Level Log Message");//Print a WARN Logger Msg
	}

}
