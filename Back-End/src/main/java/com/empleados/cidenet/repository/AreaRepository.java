package com.empleados.cidenet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleados.cidenet.model.Area;

public interface AreaRepository extends JpaRepository<Area, Long>{

}
