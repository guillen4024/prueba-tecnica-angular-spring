package com.empleados.cidenet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleados.cidenet.model.TipoDocumento;

public interface TipoDocumentoRepository extends JpaRepository<TipoDocumento, String>{

}
