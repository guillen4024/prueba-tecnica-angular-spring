package com.empleados.cidenet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleados.cidenet.model.Pais;

public interface PaisRepository extends JpaRepository<Pais, String>{

}
