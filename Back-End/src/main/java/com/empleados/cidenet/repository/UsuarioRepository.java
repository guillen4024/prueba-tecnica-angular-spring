package com.empleados.cidenet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.empleados.cidenet.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Integer>{

}
