package com.empleados.cidenet.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleados.cidenet.model.Area;
import com.empleados.cidenet.service.AreaService;

@RestController
@RequestMapping ("/api/area")
public class AreaREST {
	
	@Autowired
	private AreaService areaService;
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping
	private ResponseEntity<List<Area>> listarTodasLasArea (){
		return ResponseEntity.ok(areaService.getAllArea());
	}

}
