package com.empleados.cidenet.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleados.cidenet.model.TipoDocumento;
import com.empleados.cidenet.service.TipoDocumentoService;


@RestController
@RequestMapping ("/api/tipodocumento")
public class TipoDocumentoREST {
	
	@Autowired
	private TipoDocumentoService tipoDocumentoService;
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping
	private ResponseEntity<List<TipoDocumento>> listartTodosLosTipoDocumento (){
		return ResponseEntity.ok(tipoDocumentoService.getAllTipoDocumento());
	}
}
