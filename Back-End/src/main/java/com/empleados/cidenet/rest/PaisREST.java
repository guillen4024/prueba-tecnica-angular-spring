package com.empleados.cidenet.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.empleados.cidenet.model.Pais;
import com.empleados.cidenet.service.PaisService;

@RestController
@RequestMapping ("/api/pais")
public class PaisREST {
	
	@Autowired
	private PaisService paisService;
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping
	private ResponseEntity<List<Pais>> listarTodosLosPis (){
		return ResponseEntity.ok(paisService.getAllPais());
	}
}
