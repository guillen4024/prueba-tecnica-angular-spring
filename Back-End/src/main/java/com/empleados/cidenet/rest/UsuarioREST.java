package com.empleados.cidenet.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.empleados.cidenet.EmpleadosApplication;
import com.empleados.cidenet.model.Usuario;
import com.empleados.cidenet.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioREST {

	private static final Logger logger = LoggerFactory.getLogger(EmpleadosApplication.class);

	@Autowired
	private UsuarioService usuarioService;

	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping // ("/{pageNo}")
	private ResponseEntity<List<Usuario>> listarTodosLosUsuarios() {
		logger.info("Llamada a 'getAllUsuarios' ");
		return ResponseEntity.ok(usuarioService.getAllUsuarios());
	}
	/*
	 * private ResponseEntity<List<Usuario>> listarTodosLosUsuarios(@PathVariable
	 * Integer pageNo // @RequestParam(defaultValue = "10") Integer
	 * pageSize, @RequestParam(defaultValue = "id") String sortBy ) {
	 * logger.info("Llamada a 'getAllUsuarios', " + pageNo); return
	 * ResponseEntity.ok(usuarioService.getAllUsuarios(pageNo, 2, "id")); }
	 */

	@CrossOrigin(origins = "http://localhost:4200")
	@PostMapping
	private ResponseEntity<Usuario> agregarUsuario(@RequestBody Usuario usuario) {
		Usuario temp = usuarioService.AddUsuario(usuario);
		try {
			return ResponseEntity.created(new URI("/api/usuario" + temp.getId())).body(temp);
		} catch (Exception e) {
			return new ResponseEntity(e, HttpStatus.BAD_REQUEST);
		}
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@PutMapping("/{id}")
	public Optional<Usuario> actualizar(@PathVariable Integer id, @RequestBody Usuario usuario) {
		Optional<Usuario> temp = usuarioService.updateUsuario(id, usuario);
		try {
			return temp;
		} catch (Exception e) {
			return Optional.empty();
		}
	}

	@CrossOrigin(origins = "http://localhost:4200")
	@DeleteMapping
	private ResponseEntity<Void> eliminarUsuario(@RequestBody Usuario usuario) {
		System.out.println(usuario);
		usuarioService.deleteUsuario(usuario);
		return ResponseEntity.ok().build();
	}
	
	@CrossOrigin(origins = "http://localhost:4200")
	@GetMapping("/{doc}")
	private Usuario buscarUsuarioPorDoc(@PathVariable String doc) {
		return usuarioService.findUsuarioByDocumento(doc);
	}
}
