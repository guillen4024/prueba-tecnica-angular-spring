package com.empleados.cidenet.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "pais")
public class Pais {
	
	@Id
	private String pais_id;
	
	private String pais_nombre;
	private Boolean pais_estado;
	
	public Pais() {	}

	public Pais(String pais_id, String pais_nombre, Boolean pais_estado) {
		this.pais_id = pais_id;
		this.pais_nombre = pais_nombre;
		this.pais_estado = pais_estado;
	}

	public String getPais_id() {
		return pais_id;
	}

	public void setPais_id(String pais_id) {
		this.pais_id = pais_id;
	}

	public String getPais_nombre() {
		return pais_nombre;
	}

	public void setPais_nombre(String pais_nombre) {
		this.pais_nombre = pais_nombre;
	}

	public Boolean getPais_estado() {
		return pais_estado;
	}

	public void setPais_estado(Boolean pais_estado) {
		this.pais_estado = pais_estado;
	}
	
	
}
