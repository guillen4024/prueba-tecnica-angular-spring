package com.empleados.cidenet.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "tipo_documento")
public class TipoDocumento {
	@Id
	private String tdc_id;
	
	private String tdc_nombre;
	private Boolean tdc_estado;
	
	public TipoDocumento() {	}

	public TipoDocumento(String tdc_id, String tdc_nombre, Boolean tdc_estado) {
		this.tdc_id = tdc_id;
		this.tdc_nombre = tdc_nombre;
		this.tdc_estado = tdc_estado;
	}

	public String getTdc_id() {
		return tdc_id;
	}

	public void setTdc_id(String tdc_id) {
		this.tdc_id = tdc_id;
	}

	public String getTdc_nombre() {
		return tdc_nombre;
	}

	public void setTdc_nombre(String tdc_nombre) {
		this.tdc_nombre = tdc_nombre;
	}

	public Boolean getTdc_estado() {
		return tdc_estado;
	}

	public void setTdc_estado(Boolean tdc_estado) {
		this.tdc_estado = tdc_estado;
	}
	
	
	
}
