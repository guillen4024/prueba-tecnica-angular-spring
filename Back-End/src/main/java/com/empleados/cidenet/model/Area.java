package com.empleados.cidenet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "area")
public class Area {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int area_id;

	private String area_nombre;
	private Boolean area_estado;
	
	public Area() {	}

	public Area(int area_id, String area_nombre, Boolean area_estado) {
		this.area_id = area_id;
		this.area_nombre = area_nombre;
		this.area_estado = area_estado;
	}

	public int getArea_id() {
		return area_id;
	}

	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}

	public String getArea_nombre() {
		return area_nombre;
	}

	public void setArea_nombre(String area_nombre) {
		this.area_nombre = area_nombre;
	}

	public Boolean getArea_estado() {
		return area_estado;
	}

	public void setArea_estado(Boolean area_estado) {
		this.area_estado = area_estado;
	}
	
	
}
