package com.empleados.cidenet.model;

import java.sql.Timestamp;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "usuario")
public class Usuario {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "usu_id")
	private int id; 
	
	private String usu_apellido1;
	private String usu_apellido2;
	private String usu_nombre1;
	private String usu_nombre2;
	private String pais_id;
	private String tdc_id;
	private String usu_identificacion;
	private String usu_correo;
	private Date usu_fechaingreso;
	private int area_id;
	private Boolean usu_estado;
	private Timestamp usu_fechahoraregistro;
	private Timestamp usu_fechahoraedicion;
	
	/*Constructores*/
	public Usuario() {	}

	public Usuario(int id, String usu_apellido1, String usu_apellido2, String usu_nombre1, String usu_nombre2,
			String pais_id, String tdc_id, String usu_identificacion, String usu_correo, Date usu_fechaingreso,
			int area_id, Boolean usu_estado, Timestamp usu_fechahoraregistro, Timestamp usu_fechahoraedicion) {
		this.id = id;
		this.usu_apellido1 = usu_apellido1;
		this.usu_apellido2 = usu_apellido2;
		this.usu_nombre1 = usu_nombre1;
		this.usu_nombre2 = usu_nombre2;
		this.pais_id = pais_id;
		this.tdc_id = tdc_id;
		this.usu_identificacion = usu_identificacion;
		this.usu_correo = usu_correo;
		this.usu_fechaingreso = usu_fechaingreso;
		this.area_id = area_id;
		this.usu_estado = usu_estado;
		this.usu_fechahoraregistro = usu_fechahoraregistro;
		this.usu_fechahoraedicion = usu_fechahoraedicion;
	}

	/*Metodos Get y Set*/
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsu_apellido1() {
		return usu_apellido1;
	}

	public void setUsu_apellido1(String usu_apellido1) {
		this.usu_apellido1 = usu_apellido1;
	}

	public String getUsu_apellido2() {
		return usu_apellido2;
	}

	public void setUsu_apellido2(String usu_apellido2) {
		this.usu_apellido2 = usu_apellido2;
	}

	public String getUsu_nombre1() {
		return usu_nombre1;
	}

	public void setUsu_nombre1(String usu_nombre1) {
		this.usu_nombre1 = usu_nombre1;
	}

	public String getUsu_nombre2() {
		return usu_nombre2;
	}

	public void setUsu_nombre2(String usu_nombre2) {
		this.usu_nombre2 = usu_nombre2;
	}

	public String getPais_id() {
		return pais_id;
	}

	public void setPais_id(String pais_id) {
		this.pais_id = pais_id;
	}

	public String getTdc_id() {
		return tdc_id;
	}

	public void setTdc_id(String tdc_id) {
		this.tdc_id = tdc_id;
	}

	public String getUsu_identificacion() {
		return usu_identificacion;
	}

	public void setUsu_identificacion(String usu_identificacion) {
		this.usu_identificacion = usu_identificacion;
	}

	public String getUsu_correo() {
		return usu_correo;
	}

	public void setUsu_correo(String usu_correo) {
		this.usu_correo = usu_correo;
	}

	public Date getUsu_fechaingreso() {
		return usu_fechaingreso;
	}

	public void setUsu_fechaingreso(Date usu_fechaingreso) {
		this.usu_fechaingreso = usu_fechaingreso;
	}

	public int getArea_id() {
		return area_id;
	}

	public void setArea_id(int area_id) {
		this.area_id = area_id;
	}

	public Boolean getUsu_estado() {
		return usu_estado;
	}

	public void setUsu_estado(Boolean usu_estado) {
		this.usu_estado = usu_estado;
	}

	public Timestamp getUsu_fechahoraregistro() {
		return usu_fechahoraregistro;
	}

	public void setUsu_fechahoraregistro(Timestamp usu_fechahoraregistro) {
		this.usu_fechahoraregistro = usu_fechahoraregistro;
	}
	
	public Timestamp getUsu_fechahoraedicion() {
		return usu_fechahoraedicion;
	}

	public void setUsu_fechahoraedicion(Timestamp usu_fechahoraedicion) {
		this.usu_fechahoraedicion = usu_fechahoraedicion;
	}
	
}
