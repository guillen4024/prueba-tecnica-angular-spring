package com.empleados.cidenet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleados.cidenet.model.Area;
import com.empleados.cidenet.repository.AreaRepository;

@Service
public class AreaService {
	
	@Autowired
	private AreaRepository areaRepository;
	
	/*Metodo que retorna todas las areas que esten en estado verdadero*/
	public List<Area> getAllArea (){
		 List<Area> area = areaRepository.findAll();
		 List<Area> areaFiltro = new ArrayList<Area>(); 
		 area.forEach((a) -> {
			 if (a.getArea_estado()) {
				 areaFiltro.add(a);
			 }
		 });
		 return areaFiltro;
	}

}
