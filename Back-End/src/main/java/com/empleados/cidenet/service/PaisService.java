package com.empleados.cidenet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleados.cidenet.model.Pais;
import com.empleados.cidenet.repository.PaisRepository;

@Service
public class PaisService {
	
	@Autowired
	private PaisRepository paisRepository;
	
	/*Metodo que retorna todos los paises que esten en estado verdadero*/
	public List<Pais> getAllPais (){
		 List<Pais> pais = paisRepository.findAll();
		 List<Pais> paisFiltro = new ArrayList<Pais>(); 
		 pais.forEach((p) -> {
			 if (p.getPais_estado()) {
				 paisFiltro.add(p);
			 }
		 });
		 return paisFiltro;
	}
}
