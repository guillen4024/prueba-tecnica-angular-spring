package com.empleados.cidenet.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.empleados.cidenet.model.TipoDocumento;
import com.empleados.cidenet.repository.TipoDocumentoRepository;

@Service
public class TipoDocumentoService {
	
	@Autowired
	private TipoDocumentoRepository tipoDocumentoRepository;
	
	/*Metodo que retorna todos los tipos de documentos que esten en estado verdadero*/
	public List<TipoDocumento> getAllTipoDocumento (){
		 List<TipoDocumento> tipoDocumento = tipoDocumentoRepository.findAll();
		 List<TipoDocumento> tipoDocumentoFiltro = new ArrayList<TipoDocumento>(); 
		 tipoDocumento.forEach((td) -> {
			 if (td.getTdc_estado()) {
				 tipoDocumentoFiltro.add(td);
			 }
		 });
		 return tipoDocumentoFiltro;
	}
}
