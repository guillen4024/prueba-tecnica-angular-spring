package com.empleados.cidenet.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.empleados.cidenet.model.Usuario;
import com.empleados.cidenet.repository.UsuarioRepository;

@Service
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	/* Listar usuarios */
	public List<Usuario> getAllUsuarios() {
		return usuarioRepository.findAll();
	}
	/*
	 * public List<Usuario> getAllUsuarios(Integer pageNo, Integer pageSize, String
	 * sortBy) { Pageable paging = PageRequest.of(pageNo, pageSize,
	 * Sort.by(sortBy)); Page<Usuario> pagedResult =
	 * usuarioRepository.findAll(paging); if (pagedResult.hasContent()) { return
	 * pagedResult.getContent(); } else { return new ArrayList<Usuario>(); } }
	 */

	/* Agregar usuario */
	public Usuario AddUsuario(Usuario usuario) {
		usuario.setUsu_correo(findUsuarioCorreo(usuario.getUsu_nombre1().toLowerCase().replaceAll("\\s", ""),
				usuario.getUsu_apellido1().toLowerCase().replaceAll("\\s", ""), usuario.getPais_id().toLowerCase()));
		return usuarioRepository.save(usuario);
	}

	/* Generar correo del empleado automaticamente */
	public String findUsuarioCorreo(String nombre, String apellido, String pais) {
		String correo = nombre + "." + apellido + "@cidenet.com." + pais;
		int id = 0;
		boolean valido = true;
		while (valido) {
			valido = validarCorreo(correo);
			if (valido) {
				id++;
				correo = nombre + "." + apellido + "." + id + "@cidenet.com." + pais;
			}
		}
		return correo;
	}

	/* Valida la existencia del correo generado */
	public boolean validarCorreo(String correo) {
		List<Usuario> usu = usuarioRepository.findAll();
		boolean resp = false;
		for (Usuario u : usu) {
			if (u.getUsu_correo().trim().equals(correo)) {
				resp = true;
				break;
			} else {
				resp = false;
			}
		}
		return resp;
	}

	/*
	 * Validar existencia de un registro con el número de identificación
	 * suministrado
	 */
	public Usuario findUsuarioByDocumento(String doc) {
		List<Usuario> usu = usuarioRepository.findAll();
		Usuario usuario = new Usuario();
		for (Usuario u : usu) {
			if (u.getUsu_identificacion().trim().equals(doc.trim())) {
				usuario = u;
			}
		}
		return usuario;
	}

	/* Actualizar usuario */
	public Optional<Usuario> updateUsuario(Integer id, Usuario usuario) {
		String correo;
		if (usuario.getUsu_correo() == null) {
			correo = findUsuarioCorreo(usuario.getUsu_nombre1().toLowerCase().replaceAll("\\s", ""),
					usuario.getUsu_apellido1().toLowerCase().replaceAll("\\s", ""), usuario.getPais_id().toLowerCase());
		} else {
			correo = usuario.getUsu_correo();
		}
		return usuarioRepository.findById(id).map(usu -> {
			usu.setUsu_apellido1(usuario.getUsu_apellido1());
			usu.setUsu_apellido2(usuario.getUsu_apellido2());
			usu.setUsu_nombre1(usuario.getUsu_nombre1());
			usu.setUsu_nombre2(usuario.getUsu_nombre2());
			usu.setUsu_identificacion(usuario.getUsu_identificacion());
			usu.setTdc_id(usuario.getTdc_id());
			usu.setUsu_correo(correo);
			usu.setPais_id(usuario.getPais_id());
			usu.setUsu_fechaingreso(usuario.getUsu_fechaingreso());
			usu.setArea_id(usuario.getArea_id());
			usu.setUsu_fechahoraedicion(usuario.getUsu_fechahoraedicion());
			return usuarioRepository.save(usu);
		});
	}

	/* Eliminar usuario */
	public void deleteUsuario(Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
}
