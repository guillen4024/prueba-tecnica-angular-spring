package com.empleados.cidenet;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.sql.Date;
import java.sql.Timestamp;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.empleados.cidenet.model.Usuario;
import com.empleados.cidenet.service.UsuarioService;

@RunWith(SpringRunner.class)
@SpringBootTest
class EmpleadosApplicationTests {

	@Autowired
	private UsuarioService servicio;

	@Test
	public void contextLoads() {
		assertEquals(8, servicio.getAllUsuarios().size());
		assertThat(servicio).isNotNull();
	}

	/*@SuppressWarnings("deprecation")
	@Test
	public void guardarEmpleado() {
		Usuario usu = new Usuario();
		
		usu.setUsu_apellido1("GOMEZ");
		usu.setUsu_apellido2("ORTIZ");
		usu.setUsu_nombre1("JUAN");
		usu.setUsu_nombre2("");
		usu.setPais_id("CO");
		usu.setTdc_id("CC");
		usu.setUsu_identificacion("9876543");
		usu.setUsu_correo(null);
		usu.setUsu_fechaingreso(new Date(2021,03,06));
		usu.setArea_id(2);
		usu.setUsu_estado(true);
		usu.setUsu_fechahoraregistro(new Timestamp(2021, 03, 06, 13, 50, 10, 05)); //"2021-03-06T13:22:37.205-05:00");
		usu.setUsu_fechahoraedicion(null);

		servicio.AddUsuario(usu);
		assertThat(usu.getId()).isNotNull();
	}*/
	
	@Test 
	public void generarCorreo() {
		assertEquals("juan.perez.3@cidenet.com.co", servicio.findUsuarioCorreo("juan", "perez", "co"));
	}

}
